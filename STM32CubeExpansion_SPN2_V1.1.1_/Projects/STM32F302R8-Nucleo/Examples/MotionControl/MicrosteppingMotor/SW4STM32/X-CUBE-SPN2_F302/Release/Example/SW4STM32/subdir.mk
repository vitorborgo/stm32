################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/SW4STM32/startup_stm32f302x8.s 

C_SRCS += \
C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/SW4STM32/syscalls.c 

OBJS += \
./Example/SW4STM32/startup_stm32f302x8.o \
./Example/SW4STM32/syscalls.o 

S_DEPS += \
./Example/SW4STM32/startup_stm32f302x8.d 

C_DEPS += \
./Example/SW4STM32/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
Example/SW4STM32/startup_stm32f302x8.o: C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/SW4STM32/startup_stm32f302x8.s Example/SW4STM32/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m4 -c -x assembler-with-cpp -MMD -MP -MF"Example/SW4STM32/startup_stm32f302x8.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"
Example/SW4STM32/syscalls.o: C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/SW4STM32/syscalls.c Example/SW4STM32/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../Inc/NUCLEO-F302R8 -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/SW4STM32/syscalls.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"


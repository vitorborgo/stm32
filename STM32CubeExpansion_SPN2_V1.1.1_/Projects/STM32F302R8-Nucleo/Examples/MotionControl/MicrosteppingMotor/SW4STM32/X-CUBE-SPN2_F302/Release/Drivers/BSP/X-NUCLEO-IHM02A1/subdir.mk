################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.c \
C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8/xnucleoihm02a1_interface.c 

OBJS += \
./Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.o \
./Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1_interface.o 

C_DEPS += \
./Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.d \
./Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1_interface.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.o: C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.c Drivers/BSP/X-NUCLEO-IHM02A1/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../Inc/NUCLEO-F302R8 -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1_interface.o: C:/VDawgCores/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8/xnucleoihm02a1_interface.c Drivers/BSP/X-NUCLEO-IHM02A1/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../Inc/NUCLEO-F302R8 -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/BSP/X-NUCLEO-IHM02A1/xnucleoihm02a1_interface.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"


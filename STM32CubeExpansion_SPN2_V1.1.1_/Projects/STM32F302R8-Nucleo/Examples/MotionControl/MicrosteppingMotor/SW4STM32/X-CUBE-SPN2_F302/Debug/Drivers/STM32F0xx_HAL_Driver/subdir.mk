################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_adc.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_adc_ex.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_cortex.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_dma.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_flash.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_flash_ex.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_gpio.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_pwr.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_pwr_ex.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_rcc.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_rcc_ex.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_spi.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_uart.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_uart_ex.c 

OBJS += \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc_ex.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_cortex.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_dma.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash_ex.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_gpio.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr_ex.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc_ex.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_spi.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart.o \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart_ex.o 

C_DEPS += \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc_ex.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_cortex.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_dma.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash_ex.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_gpio.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr_ex.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc_ex.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_spi.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart.d \
./Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart_ex.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_adc.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc_ex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_adc_ex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_adc_ex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_cortex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_cortex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_cortex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_dma.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_dma.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_dma.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_flash.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash_ex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_flash_ex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_flash_ex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_gpio.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_gpio.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_gpio.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_pwr.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr_ex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_pwr_ex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_pwr_ex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_rcc.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc_ex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_rcc_ex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_rcc_ex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_spi.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_spi.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_spi.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_uart.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart_ex.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Drivers/STM32F3xx_HAL_Driver/Src/stm32f3xx_hal_uart_ex.c Drivers/STM32F0xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/STM32F0xx_HAL_Driver/stm32f3xx_hal_uart_ex.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"


################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/example.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/example_usart.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/main.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/params.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/stm32f3xx_hal_msp.c \
C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/stm32f3xx_it.c 

OBJS += \
./Example/User/example.o \
./Example/User/example_usart.o \
./Example/User/main.o \
./Example/User/params.o \
./Example/User/stm32f3xx_hal_msp.o \
./Example/User/stm32f3xx_it.o 

C_DEPS += \
./Example/User/example.d \
./Example/User/example_usart.d \
./Example/User/main.d \
./Example/User/params.d \
./Example/User/stm32f3xx_hal_msp.d \
./Example/User/stm32f3xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Example/User/example.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/example.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/example.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Example/User/example_usart.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/example_usart.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/example_usart.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Example/User/main.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/main.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/main.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Example/User/params.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/params.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/params.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Example/User/stm32f3xx_hal_msp.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/stm32f3xx_hal_msp.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/stm32f3xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Example/User/stm32f3xx_it.o: C:/VDawgsCore/STM32/STM32CubeExpansion_SPN2_V1.1.1_/Projects/STM32F302R8-Nucleo/Examples/MotionControl/MicrosteppingMotor/Src/stm32f3xx_it.c Example/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F302x8 -c -I../../../Inc -I../../../../../../../../Drivers/STM32F3xx_HAL_Driver/Inc -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../../../../../../../../Drivers/BSP/Components/L6470 -I../../../../../../../../Drivers/BSP/Components/Common -I../../../../../../../../Drivers/BSP/STM32F3xx-Nucleo -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1 -I../../../../../../../../Drivers/BSP/X-NUCLEO-IHM02A1/NUCLEO-F302R8 -O1 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Example/User/stm32f3xx_it.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"


/*
 * main_app.h
 *
 *  Created on: Aug 5, 2021
 *      Author: vitor
 */

#ifndef MOTIONCONTROL_MICROSTEPPINGMOTOR_INC_MAIN_APP_H_
#define MOTIONCONTROL_MICROSTEPPINGMOTOR_INC_MAIN_APP_H_

int mainApp(void);

#endif /* MOTIONCONTROL_MICROSTEPPINGMOTOR_INC_MAIN_APP_H_ */

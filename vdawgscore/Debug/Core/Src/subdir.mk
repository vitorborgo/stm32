################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/adc.c \
../Core/Src/eth.c \
../Core/Src/gpio.c \
../Core/Src/main.c \
../Core/Src/spi.c \
../Core/Src/stm32f7xx_hal_msp.c \
../Core/Src/stm32f7xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32f7xx.c \
../Core/Src/usart.c 

OBJS += \
./Core/Src/adc.o \
./Core/Src/eth.o \
./Core/Src/gpio.o \
./Core/Src/main.o \
./Core/Src/spi.o \
./Core/Src/stm32f7xx_hal_msp.o \
./Core/Src/stm32f7xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32f7xx.o \
./Core/Src/usart.o 

C_DEPS += \
./Core/Src/adc.d \
./Core/Src/eth.d \
./Core/Src/gpio.d \
./Core/Src/main.d \
./Core/Src/spi.d \
./Core/Src/stm32f7xx_hal_msp.d \
./Core/Src/stm32f7xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32f7xx.d \
./Core/Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/adc.o: ../Core/Src/adc.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/adc.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/eth.o: ../Core/Src/eth.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/eth.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/gpio.o: ../Core/Src/gpio.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/gpio.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/main.o: ../Core/Src/main.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/main.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/spi.o: ../Core/Src/spi.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/spi.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32f7xx_hal_msp.o: ../Core/Src/stm32f7xx_hal_msp.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32f7xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32f7xx_it.o: ../Core/Src/stm32f7xx_it.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32f7xx_it.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/syscalls.o: ../Core/Src/syscalls.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/syscalls.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/sysmem.o: ../Core/Src/sysmem.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/sysmem.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/system_stm32f7xx.o: ../Core/Src/system_stm32f7xx.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/system_stm32f7xx.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/usart.o: ../Core/Src/usart.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/usart.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"


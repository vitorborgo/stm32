################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/MotionControl/MicrosteppingMotor/Src/example.c \
../Core/MotionControl/MicrosteppingMotor/Src/example_usart.c \
../Core/MotionControl/MicrosteppingMotor/Src/main_app.c \
../Core/MotionControl/MicrosteppingMotor/Src/params.c 

OBJS += \
./Core/MotionControl/MicrosteppingMotor/Src/example.o \
./Core/MotionControl/MicrosteppingMotor/Src/example_usart.o \
./Core/MotionControl/MicrosteppingMotor/Src/main_app.o \
./Core/MotionControl/MicrosteppingMotor/Src/params.o 

C_DEPS += \
./Core/MotionControl/MicrosteppingMotor/Src/example.d \
./Core/MotionControl/MicrosteppingMotor/Src/example_usart.d \
./Core/MotionControl/MicrosteppingMotor/Src/main_app.d \
./Core/MotionControl/MicrosteppingMotor/Src/params.d 


# Each subdirectory must supply rules for building sources it contributes
Core/MotionControl/MicrosteppingMotor/Src/example.o: ../Core/MotionControl/MicrosteppingMotor/Src/example.c Core/MotionControl/MicrosteppingMotor/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/MotionControl/MicrosteppingMotor/Src/example.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/MotionControl/MicrosteppingMotor/Src/example_usart.o: ../Core/MotionControl/MicrosteppingMotor/Src/example_usart.c Core/MotionControl/MicrosteppingMotor/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/MotionControl/MicrosteppingMotor/Src/example_usart.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/MotionControl/MicrosteppingMotor/Src/main_app.o: ../Core/MotionControl/MicrosteppingMotor/Src/main_app.c Core/MotionControl/MicrosteppingMotor/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/MotionControl/MicrosteppingMotor/Src/main_app.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/MotionControl/MicrosteppingMotor/Src/params.o: ../Core/MotionControl/MicrosteppingMotor/Src/params.c Core/MotionControl/MicrosteppingMotor/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 -DSTM32F746xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1/X-NUCLEO-F746ZG" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/X-NUCLEO-IHM02A1" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/Common" -I"C:/VDawgsCore/STM32/vdawgscore/Core/BSP/Components/L6470" -I"C:/VDawgsCore/STM32/vdawgscore/Core/MotionControl/MicrosteppingMotor/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/MotionControl/MicrosteppingMotor/Src/params.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

